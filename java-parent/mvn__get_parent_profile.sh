#!/bin/bash

# 获取组件项目编译环境
# 1. master分支，环境为release
# 2. test分支，环境为test
# 3. 其他分支，环境为loc

# mvn环境变量
_PROFILE=loc

# 检测分支，如果为master则改为release
if [ "${CI_COMMIT_REF_NAME}" == "master" ] ; then
	_PROFILE=release
fi

# 检测分支，如果为test则改为test
if [ "${CI_COMMIT_REF_NAME}" == "test" ] ; then
	_PROFILE=test
fi

echo "$_PROFILE"
