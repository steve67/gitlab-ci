#!/bin/bash

#测试token = 11886854e3d2c789ae28ea329b44af19e021bd29c79754bd38e86741d0ba7fc4
#正式token = 70ff5cb3c68614b658f6fd85c09755aa4d401ac76f0fdaaed9ca24c7daed6846

_DINGTALK_TOKEN=70ff5cb3c68614b658f6fd85c09755aa4d401ac76f0fdaaed9ca24c7daed6846

if [ -z "${CI}" ] ; then
	echo "CI empty!"
	exit 99
fi

if [ -n "$1" ] ; then
	_DINGTALK_TOKEN=$1
fi

_PRJ_DIR=$(pwd)
_FILE=${_PRJ_DIR}/target/.flattened-pom.xml

_VERSION=`grep -Po '(?<=<version>).*?(?=</version>)' ${_FILE} |sed -n '1p'`

_TITLE="[parent项目发布]"

_TEXT="### 项目：${CI_PROJECT_NAME} 最新正式版已发布  \n
#### 组件包引用  \n
> &lt;parent&gt;  \n
> &nbsp;&nbsp;&lt;groupId&gt;com.huixian&lt;/groupId&gt;  \n
> &nbsp;&nbsp;&lt;artifactId&gt;huixian-parent2-infra&lt;/artifactId&gt;  \n
> &nbsp;&nbsp;&lt;version&gt;${_VERSION}&lt;/version&gt;  \n
> &lt;/parent&gt;  \n
  \n
#### 业务包引用  \n
> &lt;parent&gt;  \n
> &nbsp;&nbsp;&lt;groupId&gt;com.huixian&lt;/groupId&gt;  \n
> &nbsp;&nbsp;&lt;artifactId&gt;huixian-parent2-business&lt;/artifactId&gt;  \n
> &nbsp;&nbsp;&lt;version&gt;${_VERSION}&lt;/version&gt;  \n
> &lt;/parent&gt;  \n"

_JSON="{\"msgtype\": \"markdown\", \"markdown\": {\"title\": \"${_TITLE}\", \"text\": \"${_TEXT}\"}}"

curl -s "https://oapi.dingtalk.com/robot/send?access_token=${_DINGTALK_TOKEN}" -H 'Content-Type: application/json' --data-binary "${_JSON}"
echo
echo

exit 0
