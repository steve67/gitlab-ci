# Gitlab-CI 说明文档
- bash: 该文件夹下存放所有相关脚本
- java-ci: 存放java相关的ci配置文件
- h5-ci: 存放与H5相关的ci配置文件

其余文件夹及内容均为历史版本兼容，不再进行更新和迭代

## bash脚本说明
### variable_profile.sh
设置编译环境变量，根据分支判断当前环境：
- 默认环境：loc
- `^test$|^test//*`: test
- `^pre$|^pre//*`: pre
- `^release$|^release//*`: release (仅限组件服务使用)
- `master`: prd (仅限业务服务使用)

### variable_repository.sh
设置 maven 仓库地址:
- 默认环境：snapshot仓库
- `^master$|^pre$|^pre//*`: release仓库

### check_master_branch.sh
检测代码是否已包含最新的master分支

### check_dubbo.sh
检测Dubbo的Api接口是否符合命名规范

### build_service.sh
业务服务打包，并拷贝到指定路径下（根路径为`ROOT_PATH=/var/www/java/app/${CI_PROJECT_NAME}`）:
- 默认包路径: `${ROOT_PATH}/repo/`
- 测试包路径: `${ROOT_PATH}/test-repo/`
  - 同时构建 默认包路径 软连接
- 发布包路径: `${ROOT_PATH}/release-repo/`
  - 同时构建 默认包路径 软连接
  - 同时构建 测试包路径 软连接

### notify_business_dubbo.sh
当环境不为 `loc, dev, test` 时，执行Dubbo发布通知

### notivy_failed.sh
当任务执行失败时，执行通知，可传入通知文案