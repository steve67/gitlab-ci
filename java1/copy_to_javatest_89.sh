#!/bin/bash

_PRJ_DIR=$(pwd)

if [ -z "${CI_PROJECT_NAME}" ] ; then
	CI_PROJECT_NAME=$(basename ${_PRJ_DIR})
fi

_PRJ_DEPLOY_DIR=/var/www/java/app/${CI_PROJECT_NAME}

## 用ssh+rsync将jar包部署到 测试环境
_SRC_FILE=$(ls -t ${_PRJ_DIR}/target/${CI_PROJECT_NAME}-*.jar | head -1)
ssh root@118.190.88.89 "mkdir -vp ${_PRJ_DEPLOY_DIR}/repo/"
rsync -e ssh -vcazhP "${_SRC_FILE}" root@118.190.88.89:${_PRJ_DEPLOY_DIR}/repo/

