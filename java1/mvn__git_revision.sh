#!/bin/bash

_PRJ_DIR=$(pwd)

_GIT_TIME=$(git  --git-dir=${_PRJ_DIR}/.git/ --work-tree=${_PRJ_DIR}/  rev-list -1 --date=format-local:%Y.%m.%d --format=%cd HEAD  2>/dev/null | tail -1)
_GIT_DESCRIBE=$(git  --git-dir=${_PRJ_DIR}/.git/ --work-tree=${_PRJ_DIR}/  describe --dirty=-SNAPSHOT --match '__null__' --always  2>/dev/null)

if [ -z "${_GIT_TIME}" -o  -z "${_GIT_DESCRIBE}" ] ; then
  echo "Not in a git repository!!!"
  exit 100
fi

_REVISION=${_GIT_TIME}-${_GIT_DESCRIBE}
mvn -Drevision=${_REVISION} "$@"
