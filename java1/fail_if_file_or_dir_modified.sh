#!/bin/bash

_PRJ_DIR=$(pwd)

if [ -z "${CI_COMMIT_REF_NAME}" ] ; then
	CI_COMMIT_REF_NAME=$(git rev-parse --abbrev-ref HEAD)
fi

git fetch -vp

echo
echo ">>  HEAD  compare with  origin/${CI_COMMIT_REF_NAME}@{1}:"
git diff --name-status --exit-code origin/${CI_COMMIT_REF_NAME}@{1} -- "$@"
_exit_code=$?

if [ ${_exit_code} -eq 0 ]
then
	echo "[ $@ ] not modified!!!"
	exit 0
fi
echo

## 当前是在master分支上, 且与最新的origin/pre没有差异
if [ "${CI_COMMIT_REF_NAME}" == "master" ] ; then
	echo
	echo ">>  HEAD=(master)  compare with  origin/pre:"

	git diff --name-status --exit-code origin/pre -- "$@"
	_exit_code=$?

	if [ ${_exit_code} -eq 0 ] ; then
		echo "[ $@ ] not modified!!!"
		exit 0
	fi
	echo
fi

exit ${_exit_code}
