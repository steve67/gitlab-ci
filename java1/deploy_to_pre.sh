#!/bin/bash

_PRJ_DIR=$(pwd)

if [ -z "${CI_PROJECT_NAME}" ] ; then
	CI_PROJECT_NAME=$(basename ${_PRJ_DIR})
fi

## 创建发布到pre的目录
_PRE_PRJ_DEPLOY_DIR=/var/www/deploy_java/app/${CI_PROJECT_NAME}/repo/
mkdir -vp ${_PRE_PRJ_DEPLOY_DIR}

## 将pre的包，部署到pre（实际为拷贝到javatest指定位置）
_PRE_FILE=$(ls -t ${_PRJ_DIR}/target/${CI_PROJECT_NAME}-*.jar | head -1)
rsync -e ssh -vcazhP ${_PRE_FILE} ${_PRE_PRJ_DEPLOY_DIR}

exit 0