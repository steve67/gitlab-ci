#!/bin/bash

_DINGTALK_TOKEN=${FAIL_NOTIFY}

_DOMAIN=$1
_LAST_COMMIT=$(git log -1 HEAD^ --pretty=%H)
_COMMIT_MSG_SUBJECT=$(git log -1 --pretty=format:%s )

## 发送钉钉信息告知
_TITLE="[${CI_PROJECT_NAME} ${_PROFILE_INFO}信息]"

_TEXT="#### 项目：*${CI_PROJECT_NAME}* ${_PROFILE_INFO}信息  \n
> 域名：${_DOMAIN}  \n
> 分支：origin/${CI_COMMIT_REF_NAME}  \n
> 发布信息：${_COMMIT_MSG_SUBJECT}  \n
> 部署服务器：${HOSTNAME}  \n
> 当前版本： ${CI_COMMIT_SHA}  \n
> 上一版本： ${_LAST_COMMIT}  \n
"

_JSON="{\"msgtype\": \"markdown\", \"markdown\": {\"title\": \"${_TITLE}\", \"text\": \"${_TEXT}\"}}"

curl -s "https://oapi.dingtalk.com/robot/send?access_token=${_DINGTALK_TOKEN}" -H 'Content-Type: application/json' --data-binary "${_JSON}"
echo
echo

exit 0
