#!/bin/bash

if [[ ${_PROFILE} =~ loc|dev|test ]]; then
	echo "不进行发布通知"
    exit 0
fi

_DINGTALK_TOKEN=${DEPLOY_NOTIFY}

_PRJ_DIR=$(pwd)
_FILE=${_PRJ_DIR}/${CI_PROJECT_NAME}-dubbo/target/.flattened-pom.xml
_GROUP_ID=`grep -Po '(?<=<groupId>).*?(?=</groupId>)' ${_FILE} |sed -n '2p'`
_ARTIFACT_ID=`grep -Po '(?<=<artifactId>).*?(?=</artifactId>)' ${_FILE} |sed -n '2p'`
_VERSION=`grep -Po '(?<=<version>).*?(?=</version>)' ${_FILE} |sed -n '2p'`

_TITLE="[${CI_PROJECT_NAME}-dubbo 发布]"

_TEXT="### 项目：${CI_PROJECT_NAME} Dubbo包发布  \n
> &lt;dependency&gt;  \n
> &nbsp;&nbsp;&lt;groupId&gt;${_GROUP_ID}&lt;/groupId&gt;  \n
> &nbsp;&nbsp;&lt;artifactId&gt;${_ARTIFACT_ID}&lt;/artifactId&gt;  \n
> &nbsp;&nbsp;&lt;version&gt;${_VERSION}&lt;/version&gt;  \n
> &lt;/dependency&gt;"

_JSON="{\"msgtype\": \"markdown\", \"markdown\": {\"title\": \"${_TITLE}\", \"text\": \"${_TEXT}\"}}"

curl -s "https://oapi.dingtalk.com/robot/send?access_token=${_DINGTALK_TOKEN}" -H 'Content-Type: application/json' --data-binary "${_JSON}"
echo
echo

exit 0
