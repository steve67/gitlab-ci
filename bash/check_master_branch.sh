#!/bin/bash

_PRJ_DIR=$(pwd)

if [ -z "${CI_COMMIT_REF_NAME}" ] ; then
	CI_COMMIT_REF_NAME=$(git rev-parse --abbrev-ref HEAD)
fi

## 校验当前HEAD已经包含了最新的origin/master代码
git fetch -vp

_best_common_ancestor=$(git merge-base HEAD origin/master)
_origin_master_sha1=$(git rev-parse origin/master)

if [ "${_best_common_ancestor}" != "${_origin_master_sha1}" ]
then
	echo "_best_common_ancestor[${_best_common_ancestor}] != _origin_master_sha1[${_origin_master_sha1}]!"
	echo "please merge latest origin/master into origin/${CI_COMMIT_REF_NAME} !!!"
	
	bash /var/www/gitlab-ci/bash/notify_failed.sh "未包含 master 节点数据"

	exit 1
fi

exit 0
