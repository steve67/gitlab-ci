#!/bin/bash

_DINGTALK_TOKEN=${FAIL_NOTIFY}
_FAIL_REASON=$1

_COMMIT_MSG_SUBJECT=$(git log -1 --pretty=format:%s )

_TITLE="[${CI_PROJECT_NAME} CI任务失败]"

_TEXT="** ${GITLAB_USER_NAME} ** 提交的 [${CI_COMMIT_SHA:0:7}](${CI_PROJECT_URL}/commit/${CI_COMMIT_SHA}) 编译失败 !  \n
> 项目名称: [${CI_PROJECT_NAME}](${CI_PROJECT_URL})  \n
> 项目分支: [${CI_COMMIT_REF_NAME}](${CI_PROJECT_URL}/commits/${CI_COMMIT_REF_NAME})  \n
> 提交信息: ${_COMMIT_MSG_SUBJECT}  \n
> 原因：${_FAIL_REASON}  \n
   \n
[查看job详情](${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID})"

_JSON="{\"msgtype\": \"markdown\", \"markdown\": {\"title\": \"${_TITLE}\", \"text\": \"${_TEXT}\"}}"

curl -s "https://oapi.dingtalk.com/robot/send?access_token=${_DINGTALK_TOKEN}" -H 'Content-Type: application/json' --data-binary "${_JSON}"

exit 1
