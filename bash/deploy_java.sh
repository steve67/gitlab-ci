#!/bin/bash

# 检测是否初始化
sudo /home/linuxbrew/.linuxbrew/bin/monit status ${CI_PROJECT_NAME}
_EXIT_CODE=$?
if [ $_EXIT_CODE -ne 0 ]; then
  bash /var/www/gitlab-ci/bash/notify_failed.sh "服务未初始化"
  exit $_EXIT_CODE
fi

# 建立连接
_PRJ_DIR=/var/www/java/app/${CI_PROJECT_NAME}
_JAR_FILE_REG="${_PRJ_DIR}/repo/${CI_PROJECT_NAME}-*${CI_COMMIT_SHA:0:7}*.jar"
_JAR_FILE=$(readlink -f ${_JAR_FILE_REG} | head -1)
_LINK_TARGET=${_PRJ_DIR}/repo/$(basename ${_JAR_FILE})
_LINK_NAME=${_PRJ_DIR}/${CI_PROJECT_NAME}.jar

sudo ln -sfv -T ${_LINK_TARGET} ${_LINK_NAME}

sudo /home/linuxbrew/.linuxbrew/bin/monit restart ${CI_PROJECT_NAME}
sleep 10
sudo /home/linuxbrew/.linuxbrew/bin/monit status ${CI_PROJECT_NAME}

exit 0
