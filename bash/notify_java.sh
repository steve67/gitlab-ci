#!/bin/bash

_DINGTALK_TOKEN=${FAIL_NOTIFY}
_PRJ_DEPLOY_DIR=/var/www/java/app/${CI_PROJECT_NAME}/repo
_JAR_FILE=$(ls -t ${_PRJ_DEPLOY_DIR}/${CI_PROJECT_NAME}-*${CI_COMMIT_SHA:0:7}*.jar | head -1)
_JAR_FILE=$(sudo readlink -f ${_JAR_FILE})
if [ -z "${_JAR_FILE}" ] ; then
    bash /var/www/gitlab-ci/bash/notify_failed.sh "没有找到相关Jar包"
	exit 1
fi

_LAST_COMMIT=$(git log -1 HEAD^ --pretty=%H)

## 发送钉钉信息告知
_TITLE="[${CI_PROJECT_NAME} ${_PROFILE_INFO}信息]"

_TEXT="#### 项目：*${CI_PROJECT_NAME}* 申请${_PROFILE_INFO}信息  \n
> *分支*：origin/${CI_COMMIT_REF_NAME}  \n
> *发布版本*： ${HOSTNAME} : ${_JAR_FILE}  \n
"

_JSON="{\"msgtype\": \"markdown\", \"markdown\": {\"title\": \"${_TITLE}\", \"text\": \"${_TEXT}\"}}"

curl -s "https://oapi.dingtalk.com/robot/send?access_token=${_DINGTALK_TOKEN}" -H 'Content-Type: application/json' --data-binary "${_JSON}"
echo
echo

exit 0