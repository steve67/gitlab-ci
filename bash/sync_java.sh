#!/bin/bash

# 创建本地服务目录
_PRJ_DIR=/var/www/java/app/${CI_PROJECT_NAME}
sudo mkdir -vp ${_PRJ_DIR}/repo

# 同步文件
_JAR_FILE_REG="${_PRJ_DIR}/repo/${CI_PROJECT_NAME}-*${CI_COMMIT_SHA:0:7}*.jar"
_JAR_FILE=$(ssh root@runner "readlink -f ${_JAR_FILE_REG} | head -1")
if [ "${_JAR_FILE_REG}" == "${_JAR_FILE}" ] ; then
  bash /var/www/gitlab-ci/bash/notify_failed.sh "目标Jar包不存在"
  exit 1
fi

sudo /usr/bin/rsync -vcazhP root@runner:${_JAR_FILE} ${_PRJ_DIR}/repo

_LOCAL_JAR_FILE=$(ls -t ${_PRJ_DIR}/repo/${CI_PROJECT_NAME}-*${CI_COMMIT_SHA:0:7}*.jar | head -1)

sudo mkdir -vp /var/log/deploy/javaid_${_PROFILE}/${CI_PROJECT_NAME}
sudo chmod -R 777 /var/log/deploy/javaid_${_PROFILE}/${CI_PROJECT_NAME}
echo ${_LOCAL_JAR_FILE} >> /var/log/deploy/javaid_${_PROFILE}/${CI_PROJECT_NAME}/${_PROFILE}_deploy_info.log
echo ${_LOCAL_JAR_FILE}=${CI_COMMIT_SHA} >> /var/log/deploy/javaid_${_PROFILE}/${CI_PROJECT_NAME}/jar_map_commit.log

exit 0