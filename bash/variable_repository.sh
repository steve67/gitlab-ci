#!/bin/bash

if [[ ${CI_COMMIT_REF_NAME} =~ ^master$|^pre$|^pre//* ]]; then
    _REPO="-DaltDeploymentRepository=rdc-releases::default::https://repo.rdc.aliyun.com/repository/65774-release-AtZSVs"
else
    _REPO="-DaltDeploymentRepository=rdc-snapshots::default::https://repo.rdc.aliyun.com/repository/65774-snapshot-uLP2xj"
fi

export _REPO=$_REPO