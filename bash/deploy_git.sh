#!/bin/bash

PRJ_FILE=/var/log/deploy/site_conf/${CI_PROJECT_NAME}
if [ ! -f ${PRJ_FILE} ]; then
  bash /var/www/gitlab-ci/bash/notify_failed.sh "项目未初始化，请在/var/log/deploy/site_conf/下以项目名为文件，全路径为内容 创建文件"
  exit 1
fi

PRJ_PATH=$(tail -1 ${PRJ_FILE})
if [ ! -d ${PRJ_PATH} ]; then
  bash /var/www/gitlab-ci/bash/notify_failed.sh "找不到项目路径"
  exit 1
fi

cd ${PRJ_PATH} && sudo /home/linuxbrew/.linuxbrew/bin/git fetch -vp && sudo /home/linuxbrew/.linuxbrew/bin/git reset --hard ${CI_COMMIT_SHA}

_EXIT_CODE=$?
if [ $_EXIT_CODE -ne 0 ]
then
  bash /var/www/gitlab-ci/bash/notify_failed.sh "发布失败，原因未知"
  exit $_EXIT_CODE
fi

exit $_EXIT_CODE