#!/bin/bash

_CUR_DIR=$(cd $(dirname "${BASH_SOURCE[0]}" ) >/dev/null && pwd )
_PRJ_DIR=$(dirname ${_CUR_DIR})

if [ -z "${CI_COMMIT_REF_NAME}" ] ; then
	CI_COMMIT_REF_NAME=$(git rev-parse --abbrev-ref HEAD)
fi

git log -1 --pretty=format:%B | grep -i -F "$1"
_exit_code=$?

if [ ${_exit_code} -ne 0 ]
then
	echo
	echo "commit msg not contain < $1 >"
	echo
	exit 0
fi

exit 1
