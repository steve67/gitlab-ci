#!/bin/bash

# 获取业务包项目编译环境
# 1. master分支，环境为prd
# 2. pre分支，环境为pre
# 3. test分支，环境为test
# 4. 其他分支，环境为loc

_ENV=dev

# 检测分支，如果为test则改为test
if [ "${CI_COMMIT_REF_NAME}" == "test" ] ; then
	_ENV=test
fi

# 检测分支，如果为pre则改为pre
if [ "${CI_COMMIT_REF_NAME}" == "pre" ] ; then
	_ENV=pre
fi

# 检测分支，如果为master则改为prd
if [ "${CI_COMMIT_REF_NAME}" == "master" ] ; then
	_ENV=pdt
fi

echo "$_ENV"

exit 0
