#!/bin/bash

#测试token = 11886854e3d2c789ae28ea329b44af19e021bd29c79754bd38e86741d0ba7fc4
#正式token = 63764750b055fc9ab756c8c04bbb4a21e1f56f0a0a4ac18a6855a970f8c42f4a

_DINGTALK_TOKEN=11886854e3d2c789ae28ea329b44af19e021bd29c79754bd38e86741d0ba7fc4

if [ -z "${CI}" ] ; then
	echo "CI empty!"
	exit 99
fi

if [ -n "$1" ] ; then
	_DINGTALK_TOKEN=$1
fi

_commit_msg_subject=$(git log -1 --pretty=format:%s )

_TITLE="[测试环境部署]"

_TEXT="[[${CI_PROJECT_NAME}@${CI_COMMIT_REF_NAME}](${CI_PROJECT_URL}/commits/${CI_COMMIT_REF_NAME})] **${GITLAB_USER_NAME}** 提交的 < ${_commit_msg_subject} > * 已部署测试环境 *  \n
  \n"

_JSON="{\"msgtype\": \"markdown\", \"markdown\": {\"title\": \"${_TITLE}\", \"text\": \"${_TEXT}\"}}"

curl -s "https://oapi.dingtalk.com/robot/send?access_token=${_DINGTALK_TOKEN}" -H 'Content-Type: application/json' --data-binary "${_JSON}"
echo
echo

exit 0
