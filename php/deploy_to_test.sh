#!/bin/bash

mkdir -vp /var/log/deploy/gitid_test/${CI_PROJECT_NAME}
echo ${CI_COMMIT_SHA} >> /var/log/deploy/gitid_test/${CI_PROJECT_NAME}/test_deploy_info.log
bash /var/www/test_shell/deploy/deploy_git.sh ${CI_PROJECT_NAME}

exit 0