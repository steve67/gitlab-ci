#!/bin/bash

if [ -z "${CI_PROJECT_NAME}" ]; then
  SITES=$(tail -1 /var/log/deploy/site_conf/${CI_PROJECT_NAME})
elif
  SITES=/var/www/sites/${CI_PROJECT_NAME}
fi
echo "项目位置: ${SITES}"

cd ${SITES} && sudo /home/linuxbrew/.linuxbrew/bin/git fetch -vp && sudo /home/linuxbrew/.linuxbrew/bin/git reset --hard ${CI_COMMIT_SHA}

if [ -f "/var/www/gitlab-ci/h5/customize_script/${CI_PROJECT_NAME}.sh" ] ; then
  bash /var/www/gitlab-ci/h5/customize_script/${CI_PROJECT_NAME}.sh
fi

exit 0
