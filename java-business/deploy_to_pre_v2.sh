#!/bin/bash

_PRJ_DIR=$(pwd)

if [ -z "${CI_PROJECT_NAME}" ] ; then
	CI_PROJECT_NAME=$(basename ${_PRJ_DIR})
fi

## 创建发布到pre的目录
_PRJ_DEPLOY_DIR=/var/www/java/app/${CI_PROJECT_NAME}

_TARGET_DIR=${_PRJ_DEPLOY_DIR}/release/

# 将项目拷贝到repo文件下面
_SRC_FILE=$(ls -t ${_PRJ_DIR}/${CI_PROJECT_NAME}-service/target/${CI_PROJECT_NAME}-service-*.jar | head -1)

sudo mkdir -vp ${_TARGET_DIR}

if [ $? -eq 0 ] ; then
  sudo cp ${_SRC_FILE} ${_TARGET_DIR}
fi

sudo rsync -e ssh -vcazhP ${_SRC_FILE} ${_PRJ_RELEASE_DIR}
sudo ln -sf ${_PRJ_RELEASE_DIR}${_FILE_NAME} ${_PRJ_TEST_DIR}${_FILE_NAME}
sudo ln -sf ${_PRJ_RELEASE_DIR}${_FILE_NAME} ${_PRJ_PUBLIC_DIR}${_FILE_NAME}

exit 0