#!/bin/bash
#待废弃
_PRJ_DIR=$(pwd)

if [ -z "${CI_PROJECT_NAME}" ] ; then
	CI_PROJECT_NAME=$(basename ${_PRJ_DIR})
fi

_PRJ_DEPLOY_DIR=/var/www/java/app/${CI_PROJECT_NAME}

## 用ssh+rsync将jar包部署到 测试环境
_SRC_FILE=$(ls -t ${_PRJ_DIR}/${CI_PROJECT_NAME}-service/target/${CI_PROJECT_NAME}-service-*.jar | head -1)
ssh root@118.190.88.89 "mkdir -vp ${_PRJ_DEPLOY_DIR}/repo/"
rsync -e ssh -vcazhP "${_SRC_FILE}" root@118.190.88.89:${_PRJ_DEPLOY_DIR}/repo/


## 用`ls -t <pattern> | head -1`拿到最新的匹配文件
_JAR_FILE=$(ssh root@118.190.88.89 "ls -t ${_PRJ_DEPLOY_DIR}/repo/${CI_PROJECT_NAME}-*${CI_COMMIT_SHA:0:7}*.jar" | head -1)
if [ -z "${_JAR_FILE}" ] ; then
	echo "_JAR_FILE empty!"
	exit 1
fi

_LINK_TARGET=repo/$(basename ${_JAR_FILE})
_LINK_NAME=${_PRJ_DEPLOY_DIR}/${CI_PROJECT_NAME}.jar

ssh root@118.190.88.89 "ln -sfv -T ${_LINK_TARGET} ${_LINK_NAME}"

ssh root@118.190.88.89 "monit restart ${CI_PROJECT_NAME}"
sleep 3
ssh root@118.190.88.89 "monit status ${CI_PROJECT_NAME}"
