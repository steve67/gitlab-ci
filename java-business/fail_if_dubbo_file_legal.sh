#!/bin/bash

_PRJ_DIR=$(pwd)

if [ -z "${CI_PROJECT_NAME}" ] ; then
	CI_PROJECT_NAME=$(basename ${_PRJ_DIR})
fi

# Dubbo-api目录
_DUBBO_DIR=./${CI_PROJECT_NAME}-dubbo/src/main/java/com/huixian/*/dubbo/facade/api

# 如果文件夹不存在 返回0 允许通过
if [ ! -d ${_DUBBO_DIR} ];then
    echo "文件夹不存在，通过"
    exit 0
fi

# 统计目录下文件总数
_FILE_COUNT=`ls -l ${_DUBBO_DIR} |grep "^-" |wc -l`
# 统计目录下 Dubbo 开头的文件总数
_DUBBO_FILE_COUNT=`ls -l ${_DUBBO_DIR} |grep "Dubbo*" |wc -l`
# 统计 package-info.java 文件数量
_PACKAGE_INFO_COUNT=`ls -l ${_DUBBO_DIR} |grep "package-info.java" |wc -l`
# 累加 Dubbo开头的文件数 和 package-info.java 数量
_LEGAL_FILE_COUNT=$[$_DUBBO_FILE_COUNT+$_PACKAGE_INFO_COUNT]

# 如果文件数不相等 则返回1
if [ ${_FILE_COUNT} == ${_LEGAL_FILE_COUNT} ]; then
    exit 0
else
   echo "存在不是以Dubbo开头的文件，校验失败"
   exit 1
fi
