#!/bin/bash

_PRJ_DIR=$(pwd)

if [ -z "${CI_PROJECT_NAME}" ] ; then
	CI_PROJECT_NAME=$(basename ${_PRJ_DIR})
fi

## 创建发布到pre的目录
_PRE_PRJ_DEPLOY_DIR=/var/www/deploy_java/app/${CI_PROJECT_NAME}/repo/
ssh root@118.190.88.89 "mkdir -vp ${_PRE_PRJ_DEPLOY_DIR}"

## 将pre的包，部署到pre（实际为拷贝到javatest指定位置）
_PRE_FILE=$(ls -t ${_PRJ_DIR}/${CI_PROJECT_NAME}-service/target/${CI_PROJECT_NAME}-service-*.jar | head -1)
ssh root@118.190.88.89 "cp ${_PRE_FILE} ${_PRE_PRJ_DEPLOY_DIR}"
