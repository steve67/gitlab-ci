#!/bin/bash

#测试token = 11886854e3d2c789ae28ea329b44af19e021bd29c79754bd38e86741d0ba7fc4
#正式token = 70ff5cb3c68614b658f6fd85c09755aa4d401ac76f0fdaaed9ca24c7daed6846

_DINGTALK_TOKEN=70ff5cb3c68614b658f6fd85c09755aa4d401ac76f0fdaaed9ca24c7daed6846

if [ -z "${CI}" ] ; then
	echo "CI empty!"
	exit 99
fi

if [ -n "$1" ] ; then
	_DINGTALK_TOKEN=$1
fi

_PRJ_DIR=$(pwd)
_FILE=${_PRJ_DIR}/${CI_PROJECT_NAME}-dubbo/target/.flattened-pom.xml
_GROUP_ID=`grep -Po '(?<=<groupId>).*?(?=</groupId>)' ${_FILE} |sed -n '2p'`
_ARTIFACT_ID=`grep -Po '(?<=<artifactId>).*?(?=</artifactId>)' ${_FILE} |sed -n '2p'`
_VERSION=`grep -Po '(?<=<version>).*?(?=</version>)' ${_FILE} |sed -n '2p'`

_TITLE="[${CI_PROJECT_NAME}-dubbo发布]"

_TEXT="### 项目：${CI_PROJECT_NAME} 最新正式版已发布  \n
> &lt;dependency&gt;  \n
> &nbsp;&nbsp;&lt;groupId&gt;${_GROUP_ID}&lt;/groupId&gt;  \n
> &nbsp;&nbsp;&lt;artifactId&gt;${_ARTIFACT_ID}&lt;/artifactId&gt;  \n
> &nbsp;&nbsp;&lt;version&gt;${_VERSION}&lt;/version&gt;  \n
> &lt;/dependency&gt;  \n"

_JSON="{\"msgtype\": \"markdown\", \"markdown\": {\"title\": \"${_TITLE}\", \"text\": \"${_TEXT}\"}}"

curl -s "https://oapi.dingtalk.com/robot/send?access_token=${_DINGTALK_TOKEN}" -H 'Content-Type: application/json' --data-binary "${_JSON}"
echo
echo

exit 0
