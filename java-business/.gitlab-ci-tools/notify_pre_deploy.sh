#!/bin/bash

#测试token = 11886854e3d2c789ae28ea329b44af19e021bd29c79754bd38e86741d0ba7fc4
#正式token = 70ff5cb3c68614b658f6fd85c09755aa4d401ac76f0fdaaed9ca24c7daed6846

_DINGTALK_TOKEN=70ff5cb3c68614b658f6fd85c09755aa4d401ac76f0fdaaed9ca24c7daed6846

if [ -z "${CI}" ] ; then
	echo "CI empty!"
	exit 99
fi

if [ -n "$1" ] ; then
	_DINGTALK_TOKEN=$1
fi

_PRJ_DIR=$(pwd)

if [ -z "${CI_PROJECT_NAME}" ] ; then
	CI_PROJECT_NAME=$(basename ${_PRJ_DIR})
fi

## 用`ls -t <pattern> | head -1`拿到最新的匹配项目Jar包文件
_PRJ_DEPLOY_DIR=/var/www/deploy_java/app/${CI_PROJECT_NAME}
_JAR_FILE=$(ssh root@118.190.88.89 "ls -t ${_PRJ_DEPLOY_DIR}/repo/${CI_PROJECT_NAME}-*${CI_COMMIT_SHA:0:7}*.jar" | head -1)
if [ -z "${_JAR_FILE}" ] ; then
	echo "_JAR_FILE empty!"
	exit 1
fi

## 用`tail -1` 找到pre上次发布的信息文件
_PRE_DEPLOY_INFO_FILE=/var/log/javaid_pre/${CI_PROJECT_NAME}.log
if [ -z "${_PRE_DEPLOY_INFO_FILE}" ] ; then
	_HISTORY_JAR_FILE=""
else
  _HISTORY_JAR_FILE=$(tail -1 ${_PRE_DEPLOY_INFO_FILE})
fi

## 发送钉钉信息告知
_TITLE="[pre发布信息]"

_TEXT="#### 项目：${CI_PROJECT_NAME}申请预发布  \n
> 当前版本： /var/www/java${_JAR_FILE:20}  \n
> 上一版本： ${_HISTORY_JAR_FILE}  \n
>   \n"

_JSON="{\"msgtype\": \"markdown\", \"markdown\": {\"title\": \"${_TITLE}\", \"text\": \"${_TEXT}\"}}"

curl -s "https://oapi.dingtalk.com/robot/send?access_token=${_DINGTALK_TOKEN}" -H 'Content-Type: application/json' --data-binary "${_JSON}"
echo
echo

exit 0
