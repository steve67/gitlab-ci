#!/bin/bash

#测试token = 11886854e3d2c789ae28ea329b44af19e021bd29c79754bd38e86741d0ba7fc4
#正式token = 63764750b055fc9ab756c8c04bbb4a21e1f56f0a0a4ac18a6855a970f8c42f4a (Java告警机器人)
#正式token = 70ff5cb3c68614b658f6fd85c09755aa4d401ac76f0fdaaed9ca24c7daed6846 (Java发布机器人)

_DINGTALK_TOKEN=70ff5cb3c68614b658f6fd85c09755aa4d401ac76f0fdaaed9ca24c7daed6846

if [ -z "${CI}" ] ; then
	echo "CI empty!"
	exit 99
fi

if [ -n "$1" ] ; then
	_DINGTALK_TOKEN=$1
fi

_PRJ_DIR=$(pwd)

if [ -z "${CI_PROJECT_NAME}" ] ; then
	CI_PROJECT_NAME=$(basename ${_PRJ_DIR})
fi

## 用`ls -t <pattern> | head -1`拿到最新的匹配项目Jar包文件
_PRJ_DEPLOY_DIR=/var/www/java/app/${CI_PROJECT_NAME}
_JAR_FILE=$(ls -t ${_PRJ_DEPLOY_DIR}/repo/${CI_PROJECT_NAME}-*${CI_COMMIT_SHA:0:7}*.jar | head -1)
if [ -z "${_JAR_FILE}" ] ; then
	echo "_JAR_FILE empty!"
	exit 1
fi

## 用`tail -1` 本次需要发布的包名信息
_FAT_DEPLOY_INFO_FILE=/var/log/deploy/javaid_test/${CI_PROJECT_NAME}/test_deploy_info.log
_FAT_DEPLOY_JAR_FILE=$(tail -1 ${_FAT_DEPLOY_INFO_FILE})

_commit_msg_subject=$(git log -1 --pretty=format:%s )

_TITLE="[测试环境请求部署]"

_TEXT="[[${CI_PROJECT_NAME}@${CI_COMMIT_REF_NAME}](${CI_PROJECT_URL}/commits/${CI_COMMIT_REF_NAME})] **${GITLAB_USER_NAME}** 提交的 < ${_commit_msg_subject} > * 申请测试环境部署 *  \n
Jar包路径：${_FAT_DEPLOY_JAR_FILE}
  \n"

_JSON="{\"msgtype\": \"markdown\", \"markdown\": {\"title\": \"${_TITLE}\", \"text\": \"${_TEXT}\"}}"

curl -s "https://oapi.dingtalk.com/robot/send?access_token=${_DINGTALK_TOKEN}" -H 'Content-Type: application/json' --data-binary "${_JSON}"
echo
echo

exit 0
