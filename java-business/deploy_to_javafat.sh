#!/bin/bash
#待废弃
_PRJ_DIR=$(pwd)

if [ -z "${CI_PROJECT_NAME}" ] ; then
	CI_PROJECT_NAME=$(basename ${_PRJ_DIR})
fi

_PRJ_DEPLOY_DIR=/var/www/java/app/${CI_PROJECT_NAME}

## 用rsync将jar包部署到 测试环境
_SRC_FILE=$(ls -t ${_PRJ_DIR}/${CI_PROJECT_NAME}-service/target/${CI_PROJECT_NAME}-service-*.jar | head -1)
mkdir -vp ${_PRJ_DEPLOY_DIR}/repo/
rsync -e ssh -vcazhP ${_SRC_FILE} ${_PRJ_DEPLOY_DIR}/repo/

## 用`ls -t <pattern> | head -1`拿到最新的匹配文件
_JAR_FILE=$(ls -t ${_PRJ_DEPLOY_DIR}/repo/${CI_PROJECT_NAME}-*${CI_COMMIT_SHA:0:7}*.jar | head -1)
if [ -z "${_JAR_FILE}" ] ; then
	echo "_JAR_FILE empty!"
	exit 1
fi

## 将发布信息写入发布文件
mkdir -vp /var/log/deploy/javaid_test/${CI_PROJECT_NAME}
echo ${_JAR_FILE} >> /var/log/deploy/javaid_test/${CI_PROJECT_NAME}/test_deploy_info.log
echo ${_JAR_FILE}=${CI_COMMIT_SHA} >> /var/log/deploy/javaid_test/${CI_PROJECT_NAME}/jar_map_commit.log

exit 0
