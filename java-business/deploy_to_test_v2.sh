#!/bin/bash
_PRJ_DIR=$(pwd)

if [ -z "${CI_PROJECT_NAME}" ] ; then
        CI_PROJECT_NAME=${_PRJ_DIR}
fi

_PRJ_DEPLOY_DIR=/var/www/java/app/${CI_PROJECT_NAME}

sudo mkdir -vp ${_PRJ_DEPLOY_DIR}/repo/

_JAR_FILE=$(ssh root@runner readlink -f ${_PRJ_DEPLOY_DIR}/repo/${CI_PROJECT_NAME}-*${CI_COMMIT_SHA:0:7}*.jar | head -1)

sudo /usr/bin/rsync -vcazhP root@runner:${_JAR_FILE} ${_PRJ_DEPLOY_DIR}/repo

## 用`ls -t <pattern> | head -1`拿到最新的匹配文件
if [ -z "${_JAR_FILE}" ] ; then
    echo "_JAR_FILE empty!"
    exit 1
fi

_LOCAL_JAR_FILE=$(ls -t ${_PRJ_DEPLOY_DIR}/repo/${CI_PROJECT_NAME}-*${CI_COMMIT_SHA:0:7}*.jar | head -1)

## 将发布信息写入发布文件
sudo mkdir -vp /var/log/deploy/javaid_test/${CI_PROJECT_NAME}
sudo echo ${_LOCAL_JAR_FILE} >> /var/log/deploy/javaid_test/${CI_PROJECT_NAME}/test_deploy_info.log
sudo echo ${_LOCAL_JAR_FILE}=${CI_COMMIT_SHA} >> /var/log/deploy/javaid_test/${CI_PROJECT_NAME}/jar_map_commit.log

exit 0
