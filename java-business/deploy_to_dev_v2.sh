#!/bin/bash
_PRJ_DIR=$(pwd)

if [ -z "${CI_PROJECT_NAME}" ] ; then
        CI_PROJECT_NAME=${_PRJ_DIR}
fi

_PRJ_DEPLOY_DIR=/var/www/java/app/${CI_PROJECT_NAME}

sudo mkdir -vp ${_PRJ_DEPLOY_DIR}/repo/

_JAR_FILE=$(ssh root@runner readlink -f ${_PRJ_DEPLOY_DIR}/repo/${CI_PROJECT_NAME}-*${CI_COMMIT_SHA:0:7}*.jar | head -1)

sudo /usr/bin/rsync -vcazhP root@runner:${_JAR_FILE} ${_PRJ_DEPLOY_DIR}/repo

## 用`ls -t <pattern> | head -1`拿到最新的匹配文件

if [ -z "${_JAR_FILE}" ] ; then
    echo "_JAR_FILE empty!"
    exit 1
fi


_LINK_TARGET=repo/$(basename ${_JAR_FILE})
_LINK_NAME=${_PRJ_DEPLOY_DIR}/${CI_PROJECT_NAME}.jar

sudo ln -sfv -T ${_LINK_TARGET} ${_LINK_NAME}

ssh root@127.0.0.1 "monit restart ${CI_PROJECT_NAME}"
sleep 3
ssh root@127.0.0.1 "monit status ${CI_PROJECT_NAME}"

exit 0

