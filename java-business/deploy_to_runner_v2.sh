#!/bin/bash

_PRJ_DIR=$(pwd)

_PROFILE=`bash /var/www/gitlab-ci/java-business/variable_profile.sh`
echo "环境" + ${_PROFILE}

# 文件夹和文件名设置
_PRJ_DEPLOY_DIR=/var/www/java/app/${CI_PROJECT_NAME}
_PRJ_DEFAULT_DIR=${_PRJ_DEPLOY_DIR}/repo
_PRJ_TEST_DIR=${_PRJ_DEPLOY_DIR}/test-repo
_PRJ_RELEASE_DIR=${_PRJ_DEPLOY_DIR}/release-repo

sudo mkdir -vp ${_PRJ_DEFAULT_DIR}
sudo mkdir -vp ${_PRJ_TEST_DIR}
sudo mkdir -vp ${_PRJ_RELEASE_DIR}

# 同步文件
_SRC_FILE=$(ls -t ${_PRJ_DIR}/${CI_PROJECT_NAME}*/target/${CI_PROJECT_NAME}*.jar | head -1)
_FILE_NAME=${_SRC_FILE##*/}
if [[ ${_PROFILE} =~ ^pre$|^prd$ ]]; then
    sudo rsync -e ssh -vcazhP ${_SRC_FILE} ${_PRJ_RELEASE_DIR}
    sudo ln -sf ${_PRJ_RELEASE_DIR}/${_FILE_NAME} ${_PRJ_TEST_DIR}/${_FILE_NAME}
    sudo ln -sf ${_PRJ_RELEASE_DIR}/${_FILE_NAME} ${_PRJ_DEFAULT_DIR}/${_FILE_NAME}
elif [[ ${_PROFILE} =~ ^test$ ]]; then
    sudo rsync -e ssh -vcazhP ${_SRC_FILE} ${_PRJ_TEST_DIR}
    sudo ln -sf ${_PRJ_TEST_DIR}/${_FILE_NAME} ${_PRJ_DEFAULT_DIR}/${_FILE_NAME}
else
    sudo rsync -e ssh -vcazhP ${_SRC_FILE} ${_PRJ_DEFAULT_DIR}
fi

exit 0





